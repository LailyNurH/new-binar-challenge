package com.night.newbinarchallenge.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.night.newbinarchallenge.model.post.Order

@Dao
interface CartDao {
    @Insert
    fun insert(order: Order)

    @Query("SELECT * FROM tbl_order ORDER BY itemId DESC")
    fun getAllItem(): LiveData<List<Order>>
//
//    @Delete
//    fun delete(cart: Cart)
//
//    @Query("DELETE FROM tbl_cart WHERE itemId = :itemIdParams")
//    fun delteByItemId(itemIdParams: Long)
//
//    @Update
//    fun update(cart: Cart)

//    @Query("UPDATE tbl_food SET itemId = :newQuantity where itemId = :itemIdParams")
//    fun updateQuantityByItemId(newQuantity: Int, itemIdParams: Long)
}