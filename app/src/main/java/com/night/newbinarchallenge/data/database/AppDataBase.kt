package com.night.newbinarchallenge.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.night.newbinarchallenge.data.dao.CartDao
import com.night.newbinarchallenge.model.post.Order


@Database(entities = [Order::class], version = 1, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {

    abstract val dao: CartDao

    companion object {

        @Volatile
        private var INSTANCE: AppDataBase? = null

        fun getInstance(context: Context): AppDataBase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDataBase::class.java,
                        "db_restaurant"
                    )
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}