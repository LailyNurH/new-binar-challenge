package com.night.newbinarchallenge.model.post

data class OrderResponse(
    val code: Int,
    val message: String,
    val status: Boolean
)