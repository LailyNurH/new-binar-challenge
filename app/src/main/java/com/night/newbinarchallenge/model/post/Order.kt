package com.night.newbinarchallenge.model.post

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_order")
data class Order(
    @PrimaryKey(autoGenerate = true)
    var itemId: Long? = null,
    @ColumnInfo(name = "catatan")
    val catatan: String,
    @ColumnInfo(name = "harga")
    val harga: Int,
    @ColumnInfo(name = "nama")
    val nama: String,
    @ColumnInfo(name = "qty")
    val qty: Int
)
{
    companion object {
        const val TABLE_NAME = "tbl_order"
    }
}