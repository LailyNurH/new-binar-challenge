package com.night.newbinarchallenge.model.post

data class OrderRequest(
    val orders: List<Order>,
    val total: Int,
    val username: String
)