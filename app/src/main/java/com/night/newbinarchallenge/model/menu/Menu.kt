package com.night.newbinarchallenge.model.menu

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize

data class Menu(
    val alamat_resto: String,
    val detail: String,
    val harga: Int,
    val harga_format: String,
    val image_url: String,
    val nama: String
): Parcelable