package com.night.newbinarchallenge.model.category

data class Category(
    val image_url: String,
    val nama: String
)