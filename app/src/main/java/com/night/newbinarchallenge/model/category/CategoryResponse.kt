package com.night.newbinarchallenge.model.category

data class CategoryResponse(
    val code: Int,
    val `data`: List<Category>,
    val message: String,
    val status: Boolean
)