package com.night.newbinarchallenge.model.menu

data class ListMenuResponse(
    val code: Int,
    val `data`: List<Menu>,
    val message: String,
    val status: Boolean
)