package com.night.newbinarchallenge.network

import com.night.newbinarchallenge.model.category.CategoryResponse
import com.night.newbinarchallenge.model.menu.ListMenuResponse
import com.night.newbinarchallenge.model.post.OrderRequest
import com.night.newbinarchallenge.model.post.OrderResponse
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiEndpoint {

    @GET("category")
    fun getCategories() : Call<CategoryResponse>

    @GET("listmenu")
    fun getListMenu() : Call<ListMenuResponse>

    @GET("listmenu?")
    fun getMenuCategory(@Query("c") categoryName:String) : Call<ListMenuResponse>

    @POST("order")
    fun postOrder(@Body orderRequest: OrderRequest): Call<OrderResponse>

}