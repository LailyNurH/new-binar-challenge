package com.night.newbinarchallenge.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object BaseRetrofit {
    val api : ApiEndpoint by lazy {
        Retrofit.Builder()
            .baseUrl("https://39d1fe9a-6c43-4e47-b582-e1b1f00a9f6d.mock.pstmn.io/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiEndpoint::class.java)
    }
}