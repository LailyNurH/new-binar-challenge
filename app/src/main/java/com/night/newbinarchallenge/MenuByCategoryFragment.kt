package com.night.newbinarchallenge

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.night.newbinarchallenge.adapter.CategoryAdapter.Companion.CATEGORY_NAME
import com.night.newbinarchallenge.adapter.MenuAdapter
import com.night.newbinarchallenge.adapter.MenuCategoryAdapter
import com.night.newbinarchallenge.databinding.FragmentMenuByCategoryBinding
import com.night.newbinarchallenge.views.home.HomeViewModel


class MenuByCategoryFragment : Fragment() {
    private lateinit var binding : FragmentMenuByCategoryBinding

    private val homeMvvm: HomeViewModel by viewModels()
    private lateinit var mResultAdapter: MenuCategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding= FragmentMenuByCategoryBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mResultAdapter = MenuCategoryAdapter{
            binding.apply {
                val bundle = bundleOf("foodDetail" to it)
                findNavController().navigate(
                    R.id.action_menuByCategoryFragment_to_detailMenuFragment,
                    bundle
                )
            }

        }

        homeMvvm.MenuByCategory(CATEGORY_NAME)
//        Log.d("lIST SEARCH",searchQuery.toString())
        fetchMenuByCategory()
        prepareResultRV()
    }
    private fun fetchMenuByCategory() {
        homeMvvm.observeSearchMealsLiveData().observe(viewLifecycleOwner, Observer { mealsList ->
                    Log.d("lIST SEARCH",mealsList.toString())

            mResultAdapter.mDiffer.submitList(mealsList)
            binding.rvMenucategory.apply {

                layoutManager=  GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
                adapter = mResultAdapter
            }
        })
    }
    private fun prepareResultRV() {
//        val mResultAdapter = MenuCategoryAdapter {
//            binding.apply {
//                val bundle = bundleOf("foodDetail" to it)
//                findNavController().navigate(
//                    R.id.action_menuByCategoryFragment_to_detailMenuFragment,
//                    bundle
//                )
//            }
//
//        }
////        mResultAdapter = MenuCategoryAdapter()

    }

}