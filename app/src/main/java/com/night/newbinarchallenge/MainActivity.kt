package com.night.newbinarchallenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.night.newbinarchallenge.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupBottomNav()

    }

    private fun setupBottomNav() {
        binding.apply {
            navController = findNavController(R.id.nav_host_fragment_activity_main)

            navController.addOnDestinationChangedListener { _, destination, _ ->
                if (destination.id == R.id.homeFragment || destination.id == R.id.profileFragment || destination.id == R.id.cartFragment) {
                    binding.bottomNavView.setupWithNavController(navController)
                    binding.bottomNavView.visibility = View.VISIBLE

                } else {
                    binding.bottomNavView.visibility = View.GONE
                }
            }
        }
    }
}