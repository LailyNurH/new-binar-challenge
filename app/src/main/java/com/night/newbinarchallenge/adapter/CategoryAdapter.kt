package com.night.newbinarchallenge.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.night.newbinarchallenge.R
import com.night.newbinarchallenge.databinding.CategoryItemBinding
import com.night.newbinarchallenge.model.category.Category

class CategoryAdapter(private val onClick: (Category) -> Unit) :
    ListAdapter<Category, CategoryAdapter.CategoryViewHolder>(
        Differ()
    ) {

    class CategoryViewHolder(private val binding: CategoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun result(category: Category, onClick: (Category) -> Unit) {
            binding.apply {
                tvCategoryName.text = category.nama
                civCategory.load(category.image_url)
                root.setOnClickListener {

                    CATEGORY_NAME = category.nama.lowercase()
                    val navController = itemView.findNavController()
                    navController.navigate(R.id.action_homeFragment_to_menuByCategoryFragment)
                }


            }
        }
    }


    class Differ : DiffUtil.ItemCallback<Category>() {
        override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding =
            CategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryAdapter.CategoryViewHolder, position: Int) {
        holder.result(getItem(position), onClick)

    }

    companion object{
        var CATEGORY_NAME=""
    }
}
