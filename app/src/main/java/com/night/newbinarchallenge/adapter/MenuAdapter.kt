package com.night.newbinarchallenge.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.night.newbinarchallenge.databinding.MenuItemBinding
import com.night.newbinarchallenge.model.menu.Menu

class MenuAdapter(private val onClick: (Menu) -> Unit) :
    ListAdapter<Menu, MenuAdapter.MenuViewHolder>(
        Differ()
    ) {

    class MenuViewHolder(private val binding: MenuItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun result(menu: Menu , onClick: (Menu) -> Unit) {
            binding.apply {
                tvFoodsName.text = menu.nama
                ivFoods.load(menu.image_url)
                root.setOnClickListener {
                    onClick(menu)
                }
            }
        }
    }


    class Differ : DiffUtil.ItemCallback<Menu>() {
        override fun areItemsTheSame(oldItem: Menu, newItem: Menu): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Menu, newItem: Menu): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val binding =
            MenuItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MenuViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MenuAdapter.MenuViewHolder, position: Int) {
        holder.result(getItem(position), onClick)

    }
}