package com.night.newbinarchallenge.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.night.newbinarchallenge.databinding.MenuItemBinding
import com.night.newbinarchallenge.model.menu.Menu

class MenuCategoryAdapter(private val onClick: (Menu) -> Unit) : RecyclerView.Adapter<MenuCategoryAdapter.MenuCategoryViewHolder>() {

    private val mDiffUtil = object : DiffUtil.ItemCallback<Menu>() {
        override fun areItemsTheSame(oldItem: Menu, newItem: Menu): Boolean {
            return oldItem.nama == newItem.nama
        }

        override fun areContentsTheSame(oldItem: Menu, newItem: Menu): Boolean {
            return oldItem == newItem
        }
    }

    val mDiffer = AsyncListDiffer(this, mDiffUtil)

    inner class MenuCategoryViewHolder(
        var binding: MenuItemBinding
    ) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuCategoryViewHolder {
        return MenuCategoryViewHolder(
            MenuItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: MenuCategoryViewHolder, position: Int) {
        val meal = mDiffer.currentList[position]
        val binding = holder.binding

        binding.tvFoodsName.text = meal.nama
        binding.ivFoods.load(meal.image_url)

        holder.itemView.setOnClickListener {
            // Call the onClick lambda and pass the clicked Menu item
            onClick(meal)
        }
    }

    override fun getItemCount(): Int {
        return mDiffer.currentList.size
    }
}