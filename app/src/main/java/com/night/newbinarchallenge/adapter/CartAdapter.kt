package com.night.newbinarchallenge.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.night.newbinarchallenge.databinding.ItemCartBinding
import com.night.newbinarchallenge.model.post.Order

class CartAdapter() :
    RecyclerView.Adapter<CartItemListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartItemListViewHolder {
        return CartItemListViewHolder(
            binding = ItemCartBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CartItemListViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun setData(data : List<Order>){
        differ.submitList(data)
        notifyItemRangeChanged(0,data.size)

    }

    fun submitList(cartList: List<Order>?) {
        differ.submitList(cartList)
        if (cartList != null) {
            notifyItemRangeChanged(0,cartList.size)
        }
    }

    private val differ = AsyncListDiffer(this, object : DiffUtil.ItemCallback<Order>(){
        override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.nama == newItem.nama
        }

        override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    })


}


class CartItemListViewHolder(
    private val binding: ItemCartBinding,

//    private val onItemClick: (Cart) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(food: Order) {
        with(binding) {
            txtNamefood.text = food.nama
            txtPrice.text = "Rp ${food.harga}"
//            imgCart.load(food.)
            quantity.text = food.qty.toString()
            btnDelete.setOnClickListener {
                val itemId = food.itemId
                if (itemId != null) {
//                    cartDao.delteByItemId(itemId)
                }

            }
        }

    }
}