package com.night.newbinarchallenge.views.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.night.newbinarchallenge.model.menu.Menu

class DetailMenuViewModel : ViewModel() {
    private val _counter: MutableLiveData<Int> = MutableLiveData(0)
    val counterLiveData: LiveData<Int> get() = _counter

    private val _totalPrice = MutableLiveData<Int>()
    val totalPriceLiveData: LiveData<Int> = _totalPrice

    private val _selectedItem = MutableLiveData<Menu>()
    val selectedItemLiveData: LiveData<Menu> = _selectedItem

    fun initSelectedItem(item: Menu) {
        _selectedItem.value = item
        _totalPrice.value = item.harga
    }

    fun incrementCount() {
        _counter.value = _counter.value?.plus(1)
        updateTotalPrice()
    }

    fun decrementCount() {
        _counter.value?.let {
            if (it > 0) {
                _counter.value = _counter.value?.minus(1)
                updateTotalPrice()
            }
        }
    }

    private fun calculateTotalPrice(currentAmount: Int, selectedItem: Menu?): Int {
        return selectedItem?.harga?.times(currentAmount) ?: 0
    }

    fun updateTotalPrice() {
        val currentAmount = _counter.value ?: 1
        val selectedItem = _selectedItem.value
        if (selectedItem != null) {
            val totalPrice = selectedItem.harga * currentAmount
            _totalPrice.value = totalPrice
        }
    }
}
