package com.night.newbinarchallenge.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.night.newbinarchallenge.R
import com.night.newbinarchallenge.databinding.FragmentProfileBinding


class ProfileFragment : Fragment() {
    private lateinit var binding : FragmentProfileBinding
    private var currentUser: FirebaseUser? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentProfileBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       val mAuth = FirebaseAuth.getInstance()
        currentUser = mAuth.currentUser

        if (currentUser != null) {
            // Data profil pengguna tersedia
            val uid = currentUser!!.uid
            val name = currentUser!!.displayName // Nama pengguna
            val email = currentUser!!.email // Alamat email
            val photoUrl = currentUser!!.photoUrl // URL foto profil (jika ada)

            binding.txtUsername.text = name
            binding.txtEmail.text = email
        } else {
        }
    }

}