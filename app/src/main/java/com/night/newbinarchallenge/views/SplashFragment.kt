package com.night.newbinarchallenge.views

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.night.newbinarchallenge.R
import com.night.newbinarchallenge.databinding.FragmentSplashBinding
import com.night.newbinarchallenge.utils.SharedPreference


class SplashFragment : Fragment() {
    private lateinit var binding : FragmentSplashBinding
    private lateinit var sharedPreferencesManager: SharedPreference


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sharedPreferencesManager = SharedPreference(requireContext())

        binding = FragmentSplashBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (sharedPreferencesManager.isLoggedIn()) {
            // Pengguna sudah login, arahkan ke layar beranda
            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
        }else{
            findNavController().navigate(R.id.action_splashFragment_to_signInFragment)
        }
//        Handler(Looper.myLooper()!!).postDelayed({
//            findNavController().navigate(R.id.sp)
//
//        },3000)
    }
}