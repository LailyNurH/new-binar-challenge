package com.night.newbinarchallenge.views.details

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import coil.load
import com.night.newbinarchallenge.data.database.AppDataBase
import com.night.newbinarchallenge.databinding.FragmentDetailMenuBinding
import com.night.newbinarchallenge.model.menu.Menu
import com.night.newbinarchallenge.model.post.Order


class DetailMenuFragment : Fragment() {
    private lateinit var binding: FragmentDetailMenuBinding
    private val detailViewModel: DetailMenuViewModel by viewModels()
    var quantity : Int=0
    var totalprice : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentDetailMenuBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            detailViewModel.counterLiveData.observe(viewLifecycleOwner) { result ->
                val qty = result.toString()
                binding.tvAmount.text = qty
                quantity=qty.toInt()

            }

            detailViewModel.totalPriceLiveData.observe(viewLifecycleOwner) {
                totalprice =it
                binding.tvTotalPrice.text = "Rp"+it.toString()
            }

            incrementCount()
            decrementCount()

            var nameFood=""
             val foodDetail = arguments?.getParcelable<Menu>("foodDetail")
            foodDetail?.let {
                 ivDetailFood.load(it.image_url)
                tvFoodDescription.text = it.detail
                tvFoodPrice.text = "Rp"+it.harga.toString()
                 foodName.text = nameFood

            }
            val dataSource = AppDataBase.getInstance(requireContext()).dao
            btnAddToCart.setOnClickListener {

                dataSource.insert(Order(catatan = "", harga = totalprice, nama = nameFood, qty = quantity))
                dataSource.getAllItem().observe(requireActivity()){
                    Log.e("SimpleDataBase", it.toString())
                }
            }
        }
    }

    private fun incrementCount() {
        binding.cvPlus.setOnClickListener {
            detailViewModel.incrementCount()
            detailViewModel.updateTotalPrice()

        }
    }

    private fun decrementCount() {
        binding.cvMin.setOnClickListener {
            detailViewModel.decrementCount()
            detailViewModel.updateTotalPrice()
        }
    }


}