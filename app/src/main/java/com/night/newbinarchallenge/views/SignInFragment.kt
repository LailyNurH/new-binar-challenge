package com.night.newbinarchallenge.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.night.newbinarchallenge.R
import com.night.newbinarchallenge.databinding.FragmentSignInBinding
import com.night.newbinarchallenge.utils.SharedPreference


class SignInFragment : Fragment() {
    private lateinit var binding: FragmentSignInBinding

    private lateinit var  auth: FirebaseAuth
    private lateinit var sharedPreferencesManager: SharedPreference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        auth= FirebaseAuth.getInstance()
        sharedPreferencesManager = SharedPreference(requireContext())

        binding = FragmentSignInBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClick()
    }

    private fun onClick() {
        binding.apply {
            btnToRegister.setOnClickListener {
                findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
            }
            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password=etPassword.text.toString()

                if(email.isEmpty()||password.isEmpty()){
                    Toast.makeText(requireContext(), "Harap Isikan Semua Field!", Toast.LENGTH_SHORT).show()
                }else{
                    auth.signInWithEmailAndPassword(email,password).addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            sharedPreferencesManager.saveLoginStatus(true)
                            Toast.makeText(requireContext(), "Welcome", Toast.LENGTH_SHORT).show()

                            findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
                        }
                    }.addOnFailureListener { exception ->
                        Toast.makeText(requireContext(),exception.localizedMessage, Toast.LENGTH_LONG).show()
                    }
                }

            }
        }
    }


}