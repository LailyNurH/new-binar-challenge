package com.night.newbinarchallenge.views.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.night.newbinarchallenge.R
import com.night.newbinarchallenge.adapter.CategoryAdapter
import com.night.newbinarchallenge.adapter.MenuAdapter
import com.night.newbinarchallenge.databinding.FragmentHomeBinding
import com.night.newbinarchallenge.model.category.Category
import com.night.newbinarchallenge.model.menu.Menu




class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    private val homeMvvm: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeMvvm.getCategories()

        homeMvvm.apply {
            dataCategory.observe(viewLifecycleOwner) {
                fetchCategory(it.data)
            }
            dataMenu.observe(viewLifecycleOwner) {
                fetchListMenu(it.data)
            }
        }
    }

    private fun fetchListMenu(menu: List<Menu>) {
        val adapter = MenuAdapter {
            binding.apply {
                val bundle = bundleOf("foodDetail" to it)
                findNavController().navigate(
                    R.id.action_homeFragment_to_detailMenuFragment,
                    bundle
                )
            }

        }
        adapter.submitList(menu)
        binding.rvMenu.adapter = adapter
    }

    private fun fetchCategory(categories: List<Category>) {
        val adapter = CategoryAdapter {
        }
        adapter.submitList(categories)
        binding.rvCategory.adapter = adapter
    }

}