package com.night.newbinarchallenge.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.night.newbinarchallenge.R
import com.night.newbinarchallenge.adapter.CartAdapter
import com.night.newbinarchallenge.data.database.AppDataBase
import com.night.newbinarchallenge.databinding.FragmentCartBinding


class CartFragment : Fragment() {
    private lateinit var binding: FragmentCartBinding
    private lateinit var cartAdapter: CartAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cartRecyclerView = binding.cartRecyclerView
        val dataSource = AppDataBase.getInstance(requireContext()).dao

        binding.btnCheckout.setOnClickListener {
            showDialog()
        }

        cartAdapter = CartAdapter( )

        cartRecyclerView.adapter = cartAdapter
        cartRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        dataSource.getAllItem().observe(viewLifecycleOwner) { cartList ->
            if(cartList.isEmpty()){
                binding.llEmpty.visibility = View.VISIBLE
                binding.clPesan.visibility = View.GONE

            }else{
                binding.llEmpty.visibility = View.GONE
                binding.clPesan.visibility = View.VISIBLE

                cartAdapter.submitList(cartList)

            }
        }

    }

    private fun showDialog() {

    }
}