package com.night.newbinarchallenge.views.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.night.newbinarchallenge.model.category.CategoryResponse
import com.night.newbinarchallenge.model.menu.ListMenuResponse
import com.night.newbinarchallenge.model.menu.Menu
import com.night.newbinarchallenge.network.BaseRetrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel(){

    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val status: MutableLiveData<String> = MutableLiveData()
    private var mSearchMealsLiveData = MutableLiveData<List<Menu>>()


    private val mCategoriesLiveData: MutableLiveData<CategoryResponse> by lazy {
        MutableLiveData<CategoryResponse>().also {
            getCategories()
        }
    }
    val dataCategory: LiveData<CategoryResponse> = mCategoriesLiveData

    private val mListMenuLiveData: MutableLiveData<ListMenuResponse> by lazy {
        MutableLiveData<ListMenuResponse>().also {
            getListMenu()
        }
    }
    val dataMenu: LiveData<ListMenuResponse> = mListMenuLiveData

    private val mListMenuCategory: MutableLiveData<ListMenuResponse> by lazy {
        MutableLiveData<ListMenuResponse>().also {
//            getMenuByCategory(ca)
        }
    }

     fun getCategories() {
        loading.postValue(true)
        BaseRetrofit.api.getCategories().enqueue(object : Callback<CategoryResponse> {
            override fun onResponse(
                call: Call<CategoryResponse>,
                response: Response<CategoryResponse>
            ) {
                loading.postValue(false)
                when {
                    response.code() == 200 -> mCategoriesLiveData.postValue(response.body())
                    else -> status.postValue("Error")
                }
            }

            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {
                Log.d("categoryError", t.message.toString())
            }

        })
    }

    fun getListMenu() {
        loading.postValue(true)
        BaseRetrofit.api.getListMenu().enqueue(object : Callback<ListMenuResponse> {
            override fun onResponse(
                call: Call<ListMenuResponse>,
                response: Response<ListMenuResponse>
            ) {
                loading.postValue(false)
                when {
                    response.code() == 200 -> mListMenuLiveData.postValue(response.body())
                    else -> status.postValue("Error")
                }
            }

            override fun onFailure(call: Call<ListMenuResponse>, t: Throwable) {
                Log.d("categoryError", t.message.toString())
            }

        })
    }

    fun MenuByCategory(categoryName: String) = BaseRetrofit.api.getMenuCategory(categoryName)
        .enqueue(object : Callback<ListMenuResponse> {
            override fun onResponse(call: Call<ListMenuResponse>, response: Response<ListMenuResponse>) {
                val mealList = response.body()?.data
                mealList?.let {
                    mSearchMealsLiveData.postValue(it)
                    Log.d("search", it.toString())

                }
            }

            override fun onFailure(call: Call<ListMenuResponse>, t: Throwable) {
                Log.d("Meals Search ", t.message.toString())
            }
        })
    fun observeSearchMealsLiveData(): LiveData<List<Menu>> = mSearchMealsLiveData

}